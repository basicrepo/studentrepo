package com.afrid.service;

import java.util.List;

import com.afrid.entity.Student;

public interface StudentService {
	
	//create student details
	public Student addStudentDetails(Student student);
	
	
	//get Students details by id
		public Student selectOneStudent(long id ); 
		
	//get all students
	public List<Student> selectAllStudent();
	
	
	
	//delete student by id
	public void deleteStudentById(long id);
	
	
	
	//update student details by id
	public Student updateStudentDetails(long id,Student student);
	
	//update student name
	public Student updateStudentMail(long id, String mail);

}
