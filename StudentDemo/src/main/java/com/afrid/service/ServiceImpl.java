package com.afrid.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.afrid.entity.Student;
import com.afrid.exception.StudentNotFoundException;
import com.afrid.repo.StudentRepo;

@Service
public class ServiceImpl implements StudentService {

	@Autowired
	public StudentRepo repo;
	
	
	@Override
	public Student addStudentDetails(Student student) {
		
		Student s= repo.save(student);
		return s;
	}
	
	@Override
	public List<Student> selectAllStudent() {
		
		return repo.findAll();
	}

	@Override
	public Student selectOneStudent(long id) {
		// TODO Auto-generated method stub
		return repo.findById(id).orElseThrow(()-> new StudentNotFoundException(id+ " No student present"));
	}

	@Override
	public void deleteStudentById(long id) {
		if(repo.existsById(id))
		{ repo.deleteById(id);}
		else 
		{throw new StudentNotFoundException(id+" Not found");
		}
		
		
	}

	

	@Override
	public Student updateStudentDetails(long id, Student student) {
		if(repo.existsById(id)) {
			student.setId(id);
			return repo.save(student);
		}
		else {
			throw new StudentNotFoundException(id+" This Student not id is not present please check once again"); 
		}
		
		
	}

	@Override
	public Student updateStudentMail(long id, String mail) {
		Optional<Student> optionalStudent = repo.findById(id);
	    if (optionalStudent.isPresent()) {
	        Student student = optionalStudent.get();
	        student.setEmail(mail);
	        return repo.save(student);
	    } else {
	        throw new StudentNotFoundException("Student not found with id: " + id);
	    }
		
	}

}
