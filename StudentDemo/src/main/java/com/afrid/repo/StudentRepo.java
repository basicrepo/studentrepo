package com.afrid.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.afrid.entity.Student;
@Repository
public interface StudentRepo extends JpaRepository<Student, Long>{

}
