package com.afrid;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afrid.entity.Student;
import com.afrid.service.ServiceImpl;

@RestController
@RequestMapping("/st")
public class StudentApi {
	
	@Autowired
	public ServiceImpl serviceImpl;
	
	
	//save student
	@PostMapping
	public ResponseEntity<Student> addStudentDetails(@RequestBody Student student){
		
		Student s= serviceImpl.addStudentDetails(student);
		return new ResponseEntity<>(s,HttpStatus.ACCEPTED);
	}
	
	
	
	//get details by id
	@GetMapping("/{id}")
	public ResponseEntity<Student> selectStudentById(@PathVariable long id){
		return new ResponseEntity<> (serviceImpl.selectOneStudent(id),HttpStatus.OK);
		
	}
	
	// get all student
	@GetMapping
    public ResponseEntity<List<Student>> getAllStudents() {
        return new ResponseEntity<>(serviceImpl.selectAllStudent(),HttpStatus.OK);
    }
	
	//delete student by id
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteStudent(@PathVariable("id") long id)
	{
		serviceImpl.deleteStudentById(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	//update details by id
	@PutMapping("/{id}")
	public ResponseEntity<Student> updateStudentDetails(@PathVariable long id,@RequestBody Student student){
		return new ResponseEntity<>(serviceImpl.updateStudentDetails(id, student),HttpStatus.ACCEPTED);
	}
	
	
	@PatchMapping("/{id}/updatemail")
	public ResponseEntity<Student> updateMail(@PathVariable long id, @RequestBody Map<String, String> requestBody){
		String email = requestBody.get("email");
		return new ResponseEntity<>(serviceImpl.updateStudentMail(id, email),HttpStatus.ACCEPTED);
	}

}
